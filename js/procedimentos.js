class Calculos {
    constructor() {

    }
    decimalAOtrasBases(cantidad, base) {
        let resultado;
        switch (base) {
            case '2':
                resultado = cantidad.toString(2);
                break;
            case '8':
                resultado = cantidad.toString(8);
                break;
            case '10':
                resultado = cantidad.toString(10);
                break;
            case '16':
                resultado = cantidad.toString(16);
                break;
        }
        return resultado;
    }
    //CANTIDAD:String -BASE:int -BASEFINAL:int
    binarioaOtrasBases(cantidad,base,baseFinal){
        const resultado=parseInt(cantidad,base).toString(baseFinal);
        return resultado;
    }
}