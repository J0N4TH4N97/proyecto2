class Interfaz {
    constructor() { }
    mostrarMensaje(msj, tipo) {
        const divMsj = document.querySelector('#resultado');
        const div = document.createElement('div');
        div.classList.add('text-center', 'alert');
        if (tipo === 'error') {
            div.classList.add('alert-danger');
        } else {
            div.classList.add('alert-success');
        }

        div.appendChild(document.createTextNode(msj));
        divMsj.appendChild(div);
        setTimeout(() => {
            document.querySelector('#resultado .alert').remove();
        }, 3000);
    }
    mostrarResultado(cantidad,base,baseFinal,resultado) {     
        const div=document.createElement('div');
        div.classList.add('text-center','alert');
        div.classList.add('alert-success');

        div.innerHTML=`<p>Cantidad a Convertir: ${cantidad} Base: ${base} </p><button type="button" class="btn btn-primary">Resultado: <span class="badge badge-light">${resultado}</span> Base: ${baseFinal}</button>`;

        document.querySelector('#resultado').appendChild(div);
    }
}
