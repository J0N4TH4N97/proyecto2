const formulario = document.getElementById('convertir-base');
const ui = new Interfaz();
const calculo = new Calculos();
//eventos
formulario.addEventListener('submit', (e) => {
    e.preventDefault();
    let div=document.querySelector('#resultado div')
    if(div){
        div.remove();
    }
    const cantidad = document.getElementById('cantidad').value;
    const selectBases2 = document.querySelector('#bases2');
    const selectBases = document.querySelector('#bases');
    const baseSeleccionada = selectBases.options[selectBases.selectedIndex].value;
    const baseDeseada = selectBases2.options[selectBases2.selectedIndex].value;

    if (cantidad === '' || baseSeleccionada === '' || baseSeleccionada === 'Bases' || baseDeseada === 'Base a La Que Desea Convertir') {
        ui.mostrarMensaje('Debe Ingresar la Cantidad, su Base y la Base a la que desea Convertir', 'error');
    } else {
        ui.mostrarMensaje('Calculando...', 'correcto');
        const cantidadInt = parseInt(cantidad);
        const baseSeleccionadaInt = parseInt(baseSeleccionada);
        const baseDeseadaInt = parseInt(baseDeseada);
        //comparar cual base es la que insera el usuario
        let resultado;
        switch (baseSeleccionada) {
            case '2':
                resultado = calculo.binarioaOtrasBases(cantidad, baseSeleccionadaInt, baseDeseadaInt)
                ui.mostrarResultado(cantidad,baseSeleccionada,baseDeseada,resultado);
                break;
            case '8':
                resultado = calculo.binarioaOtrasBases(cantidad, baseSeleccionadaInt, baseDeseadaInt);
                ui.mostrarResultado(cantidad,baseSeleccionada,baseDeseada,resultado);
                break;
            case '10':
                resultado = calculo.decimalAOtrasBases(cantidadInt, baseDeseada);
                ui.mostrarResultado(cantidad,baseSeleccionada,baseDeseada,resultado);
                break;
            case '16':
                resultado = calculo.binarioaOtrasBases(cantidad, baseSeleccionadaInt, baseDeseadaInt);
                ui.mostrarResultado(cantidad,baseSeleccionada,baseDeseada,resultado);
                break;
        }
    }
})
